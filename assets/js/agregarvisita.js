
let vehiculo = JSON.parse(sessionStorage.getItem("vehicle"));

if (vehiculo == null) {
    alert('Ningun Vehiculo Seleccionado');
    window.location.replace(window.location.origin + '/UI/index.html')
} else {

    $(document).ready(function () {
        $.ajax({
            crossOrigin: true,
            url: url_base + "/api/Vehicles/" + vehiculo.vehicleId,
            accepts: "application/json",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + token
            },
            success: function (data) {
                console.log(data);
                setVehiculo(data);
            }
            , error: function (err) {
                console.log(err);
            }
        });
    });

    function setVehiculo(data) {
        $("#plate").text(data.plate);
        $("#brand").text(data.brand);
        $("#model").text(data.model);
        $("#year").text(data.year.substr(0,4));
        sessionStorage.setItem("vehicle",JSON.stringify(data));
        vehiculo=data;

    }

    function agregarVisita() {
        $.ajax({
            method: 'POST',
            crossOrigin: true,
            url: url_base + "/api/Visits",
            accepts: "application/json",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + token
            },
            data: JSON.stringify({
                "vehicleId": vehiculo.vehicleId,
                "visitDate": $("#visitDate").val()+ "T00:00:00",
                "visitStatus": 0,
                "price": 0
            }),
            success: function (data) {
                console.log(data);
                window.location.replace(window.location.origin + '/UI/menu/visitas.html')
            },
            error: function (err) {
                console.error(err);
            }
        });
    }

}
