
//const $ = require('jquery');
let vehicle = JSON.parse(sessionStorage.getItem('vehicle'));
let visit = JSON.parse(sessionStorage.getItem('visit'));;
let items = [];


if (visit == null) {
    alert('Ningun Vehiculo Seleccionado');
    window.location.replace(window.location.origin + '/UI/index.html')
}
else {
    $(document).ready(function () {
        $.ajax({
            crossOrigin: true,
            url: url_base + "/api/Vehicles/" + visit.vehicleId,
            accepts: "application/json",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + token
            },
            success: function (data) {
                console.log(data);
                setVehicle(data);
            },
            error: function (err) {

            }
        });

        $.ajax({
            crossOrigin: true,
            url: url_base + "/api/Visits/" + visit.visitId,
            accepts: "application/json",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + token
            },
            success: function (data) {
                console.log(data);
                setVisit(data);
            },
            error: function (err) {

            }
        });

    });

    function setVehicle(data) {
        vehicle = data;
        vehicle.visits = null;
        sessionStorage.setItem('vehicle', JSON.stringify(vehicle));
        $("#plate").text(vehicle.plate);
        $("#model").text(vehicle.model);
        $("#year").text(vehicle.year.substr(0, 4));
    }

    function setVisit(data) {
        visit = data;
        $("#visitDate").text(visit.visitDate);
        $("#visitStatus").text(visit.visitStatus);
        visit.services.forEach(service => {
            items.push(service.item);
            $('#items').append(`<tr><td>${service.item.pieceName}</td><td>${service.item.price}</td><td><button onclick="deleteItem(this)" itemId="${service.item.itemId}" type="button" class="btn btn-danger">Remover</button></td></tr>`);
        });
        visit.services = null;
        sessionStorage.setItem('visit', JSON.stringify(visit));
        updatePrice()

    }

    function updatePrice(){
        let price = 0.00;
        items.forEach(item => {
            price = price + parseFloat(item.price)
        });
        console.log(price)
        $("#price").text(price);
    }

   function deleteItem(e) {
        $.ajax({
            method: 'DELETE',
            crossOrigin: true,
            url: url_base + "/api/Visits/" + visit.visitId + "/"+ e.attributes.itemid.value,
            accepts: "application/json",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + token
            },
            success: function (data) {
                console.log(data);
                location.reload();
            },
            error: function (err) {
                console.error(err);
            }
        });
    }

}