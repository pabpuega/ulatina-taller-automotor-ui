
let vehiculo = JSON.parse(sessionStorage.getItem("vehicle"));
let visita = JSON.parse(sessionStorage.getItem("visit"));

if (visita == null) {
    alert('Ningun Vehiculo Seleccionado');
    window.location.replace(window.location.origin + '/UI/index.html')
} else {

    $(document).ready(function () {
        $.ajax({
            crossOrigin: true,
            url: url_base + "/api/items",
            accepts: "application/json",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + token
            },
            success: function (data) {
                console.log(data);
                opc(data);
            }
            , error: function (err) {
                console.log(err);
            }
        });
    });

    function opc(data) {
        data.forEach(item => {
            $("#repuesto").append(`<option value="${item.itemId}">${item.pieceName}</option>`)
        });

    }

    $(document).ready(function () {
        $.ajax({
            crossOrigin: true,
            url: url_base + "/api/Vehicles/" + visita.vehicleId,
            accepts: "application/json",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + token
            },
            success: function (data) {
                console.log(data);
                datosVehiculo(data);
            }
            , error: function (err) {
            }
        });
    });

    function datosVehiculo(data) {
        $("#plate").val(`${data.plate}`)
        $("#model").val(data.model)
        $("#year").val(data.year.substr(0,4))
    }

    function agregar() {
        $.ajax({
            method: 'POST',
            crossOrigin: true,
            url: url_base + "/api/Visits/" + visita.visitId + "/"+ $("#repuesto").val(),
            accepts: "application/json",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + token
            },
            success: function (data) {
                console.log(data);
                window.location.replace(window.location.origin + '/UI/menu/visita.html')
            },
            error: function (err) {
                console.error(err);
            }
        });
    }

}
