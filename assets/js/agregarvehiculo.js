
function agregar() {
    $.ajax({
        method: 'POST',
        crossOrigin: true,
        url: url_base + "/api/Vehicles",
        accepts: "application/json",
        contentType: "application/json",
        headers: {
            "Authorization": "Bearer " + token
        },
        data: JSON.stringify( {
            "plate": $("#plate").val(),
            "brand": $("#brand").val(),
            "model": $("#model").val(),
            "year": $("#year").val()+"-01-01T00:00:00"
        }),
        success: function (data) {
            console.log(data);
            window.location.replace(window.location.origin + '/UI/menu/vehiculos.html');
        },
        error: function (err) {
            console.error(err);
        }
    });
}

