//const $ = require('jquery');
const username = $('#userName'); //Obtener del form
const password = $('#password'); //Obtener del form
 
$('#loginBtn').click((event) => {
    event.preventDefault();
    inisiarSesion();
});
 
function inisiarSesion() {
    let path = "/api/account/login"
    $.ajax({
        method: 'POST',
        crossOrigin: true,
        url: url_base + path,
        accepts: "application/json",
        contentType: "application/json",
        data: JSON.stringify({
            "userName": username.val(), 
            "password": password.val()
        }),
        success: function(data) {
            console.log(data);
            setLogin(data);
           window.location.replace(window.location.origin + '/UI/index.html')
        },
        error: function(err) {
            console.error(err);
           location.reload();
        }
    });
}
 
function setLogin(data) {
    sessionStorage.setItem('token',data.token);
    sessionStorage.setItem('expiration',data.expiration);
}