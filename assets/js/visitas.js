let vehicle = JSON.parse(sessionStorage.getItem("vehicle"));
let visits = [];
console.log(vehicle);
//const $ = require('jquery');

if (vehicle == null) {
    alert('Ningun Vehiculo Seleccionado');
    window.location.replace(window.location.origin + '/UI/index.html');
}
else {
    console.log(vehicle.vehicleId);
    $(document).ready(function () {
        $.ajax({
            crossOrigin: true,
            url: url_base + "/api/Vehicles/" + vehicle.vehicleId,
            accepts: "application/json",
            contentType: "application/json",
            headers: {
                "Authorization": "Bearer " + token
            },
            success: function (data) {
                console.log(data);
                setVehicle(data);
            },
            error: function (err) {

            }
        });

    });


    function setVehicle(data) {
        console.log(data);
        vehicle = data;
        visits = vehicle.visits;
        vehicle.visits = null;
        sessionStorage.setItem('vehicle', JSON.stringify(vehicle));
        $("#plate").text(vehicle.plate);
        $("#model").text(vehicle.model);
        $("#year").text(vehicle.year.substr(0, 4));
        visits.forEach(visit => {
            $('#visits').append(`<tr><td>${visit.visitDate.substr(0,10)}</td><td>${visit.visitStatus}</td><td>${visit.price}</td><td><button onclick="toDetails(this)" visitId="${visit.visitId}" type="button" class="btn btn-secundary">Detalles</button></td></tr>`);
        });
    }

    function toDetails(e) {
        for (let i = 0; i < visits.length; i++) {
            if (visits[i].visitId == e.attributes.visitId.value) {
                sessionStorage.setItem('visit',JSON.stringify(visits[i]));
            }
        };
        window.location.replace(window.location.origin + '/UI/menu/visita.html');
    }
    
}