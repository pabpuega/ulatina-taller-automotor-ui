
(function () {
    console.log('validate session');
    if (window.location.pathname != '/UI/login/login.html') {
        if (sessionStorage.getItem('expiration') != null) {
            if (new Date(sessionStorage.getItem('expiration')) >= Date.now()) {
                return;
            }
        }
    }else{
        return;
    }
    window.location.replace(window.location.origin + '/UI/login/login.html')
}) ();

const url_base = "http://localhost:5000"
const token = sessionStorage.getItem('token');

