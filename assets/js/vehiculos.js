let vehicles= [];
$(document).ready(function () {
    $.ajax({
        crossOrigin: true,
        url: url_base + "/api/Vehicles",
        accepts: "application/json",
        contentType: "application/json",
        headers: {
            "Authorization": "Bearer " + token
        },
        success: function (data) {
            console.log(data);
            setVehicles(data);
        },
        error: function (err) {

        }
    });

});


function setVehicles(data) {
    vehicles=data;
    vehicles.forEach(vehicle => {
        $('#vehicles').append(`<tr><td>${vehicle.plate}</td><td>${vehicle.brand}</td><td>${vehicle.model}</td><td>${vehicle.year.substr(0,4)}</td><td><button onclick="toDetails(this)" vehicleId="${vehicle.vehicleId}" type="button" class="btn btn-secundary">Detalles</button></td></tr>`);
    });
}

function toDetails(e) {
    for (let i = 0; i < vehicles.length; i++) {
        if (vehicles[i].vehicleId == e.attributes.vehicleId.value) {
            console.log(vehicles[i]);
            sessionStorage.setItem('vehicle',JSON.stringify(vehicles[i]));
        }
    };
    window.location.replace(window.location.origin + '/UI/menu/visitas.html');
}